// À FAIRE
// date format
// mise en page pcq trop le bordel avec le dom
// filmPageLink a définir
// cardText : overview + date

import '../node_modules/bootstrap/js/dist/index.js'

// // trop long
// let input = document.getElementById('input')
// input.setAttribute('size', input.getAttribute('placeholder').length)

/**
 * API key
 */
const CLE = 'api_key=2555f3818ce8a1ae9bd523b7e622bd8d'

/**
 * API img base_url
 */
let baseUrl = 'https://image.tmdb.org/t/p/'

/**
 * input for search
 */
let searchInput = document.querySelector('#search-input')

/**
 * submit btn
 */
let searchBtn = document.querySelector('#search-btn')




/**
 * get a list of films from the API, matching with the user input value
 * @param {} event
 */
searchBtn.onclick = function (event) {
    event.preventDefault()
    /**
     * user value of the search input 
     */
    const searchValue = searchInput.value
    let url = `https://api.themoviedb.org/3/search/movie/?${CLE}&query=${searchValue}`
    console.log(url)

    fetch(url)
        // .then(res => res.json())
        .then(res => {
            return res.json()
        })
        .then((data) => {
            console.log('data: ', data)

            // switching between 'main home' and 'main results'
            let mainResults = document.querySelector('#results')
            mainResults.classList.remove('d-none')
            let mainHome = document.querySelector('#home')
            mainHome.classList.remove('d-flex', 'flex-column', 'align-items-center')
            // flex-column align-items-center
            mainHome.classList.add('d-none')

            /**
             * Browsing results
             */
            for (let i = 0; i < data.results.length; ++i) {
                // let poster = document.createElement('img')
                // let title = document.createElement('p')
                // let overview = document.createElement('p')
                // let date = new Date()
                // let date = document.createElement('p')

                // --------------- CREATION CARDS FILMS & displaying results inside

                let filmCard = document.createElement('article')
                filmCard.classList.add('col-12', 'col-md-4', 'card')
                filmCard.style.width = '18rem'

                let cardPoster = document.createElement('img')
                cardPoster.classList.add('card-img-top')

                /**
                 * if poster or no poster for this film
                 */
                if (data.results[i].poster_path == null) {
                    cardPoster.src = '../src/img/no_pic2.jpeg'
                    // TEXT VERSION:
                    //     cardPoster = document.createElement('p')
                    //     cardPoster.innerText = `(There's no poster for this film.)`
                    //     cardPoster.classList.add('font-italic')
                }
                else {
                    cardPoster.src = `${baseUrl}w300${data.results[i].poster_path}`
                }

                let cardBody = document.createElement('div')
                cardBody.classList.add('card-body')

                let cardTitle = document.createElement('h3')
                cardTitle.classList.add('card-title')

                let cardText = document.createElement('p')
                cardText.classList.add('card-text')

                let filmPageBtn = document.createElement('a')
                filmPageBtn.href = '#'
                // filmPageBtn.classList('btn' , 'btn-primary')

                mainResults.appendChild(filmCard)
                filmCard.appendChild(cardPoster)
                filmCard.appendChild(cardBody)
                filmCard.appendChild(cardTitle)
                filmCard.appendChild(cardText)
                filmCard.appendChild(filmPageBtn)

                let searchTitle = document.querySelector('#search-title')
                searchTitle.innerText = `Results for '${searchValue}'`

                cardTitle.innerText = data.results[i].title
                cardPoster.alt = `Poster of ${cardTitle.innerText}`
                cardText.innerText = data.results[i].overview
                // add date inside
            }
        })
}
